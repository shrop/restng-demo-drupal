<?php
/**
 * @file
 * restng_config.ds.inc
 */

/**
 * Implements hook_ds_field_settings_info().
 */
function restng_config_ds_field_settings_info() {
  $export = array();

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|product|teaser';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'product';
  $ds_fieldsetting->view_mode = 'teaser';
  $ds_fieldsetting->settings = array(
    'title' => array(
      'weight' => '3',
      'label' => 'hidden',
      'format' => 'default',
      'formatter_settings' => array(
        'link' => '1',
        'wrapper' => 'h2',
        'class' => 'text-overflow page-header',
      ),
    ),
    'node_link' => array(
      'weight' => '2',
      'label' => 'hidden',
      'format' => 'default',
      'formatter_settings' => array(
        'link text' => 'Buy Now',
        'wrapper' => 'h3',
        'class' => 'btn btn-primary',
      ),
    ),
  );
  $export['node|product|teaser'] = $ds_fieldsetting;

  return $export;
}

/**
 * Implements hook_ds_layout_settings_info().
 */
function restng_config_ds_layout_settings_info() {
  $export = array();

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|product|teaser';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'product';
  $ds_layout->view_mode = 'teaser';
  $ds_layout->layout = 'ds_2col_stacked';
  $ds_layout->settings = array(
    'regions' => array(
      'left' => array(
        0 => 'field_image',
        1 => 'field_price',
        2 => 'node_link',
      ),
      'right' => array(
        3 => 'title',
        4 => 'body',
      ),
    ),
    'fields' => array(
      'field_image' => 'left',
      'field_price' => 'left',
      'node_link' => 'left',
      'title' => 'right',
      'body' => 'right',
    ),
    'limit' => array(
      'field_image' => '1',
    ),
    'classes' => array(
      'layout_class' => array(
        'col-md-4 col-sm-6' => 'col-md-4 col-sm-6',
        'well' => 'well',
      ),
    ),
    'wrappers' => array(
      'header' => 'div',
      'left' => 'div',
      'right' => 'div',
      'footer' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => 'content',
    'layout_link_custom' => '',
  );
  $export['node|product|teaser'] = $ds_layout;

  return $export;
}
