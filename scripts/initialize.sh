#!/bin/bash

if [ ! -f "settings.php" ]; then
  echo "You must run this from a Drupal site directory containing settings.php"
  exit 1
fi

if [ ! -d "modules/contrib" ]; then
  chmod +w .
  drush make --no-core --contrib-destination=. drush.make .
fi

drush site-install --site-name='Drupal RestNG'

#disable modules we don't like, and enable the ones we do like
drush pm-disable -y update,overlay,dashboard,comment,rdf,toolbar,shortcut,search
drush pm-enable  -y admin_menu,devel,module_filter,devel_generate,admin_menu_toolbar

#compile SASS to CSS for our custom theme
(cd themes/restng_bootstrap && compass compile)

#enable the restng_bootstrap theme
drush -y en restng_bootstrap
drush vset theme_default restng_bootstrap

#enable the config feature
drush -y en restng_config

#the feature needs reverting
#because certain variables go back to their default values
drush -y features-revert restng_config

#add packaged content (node_export_features must be enabled BEFORE the content feature)
#drush -y en node_export_features
#drush -y en drupal_restng_content

#generate catalog terms
drush generate-terms catalog 10

#generate articles
drush generate-content --types=product 50 0

#disable certain blocks
drush sql-query "UPDATE block SET status = 0 WHERE module = 'system' AND delta = 'navigation' AND theme = 'restng_bootstrap'"
drush sql-query "UPDATE block SET status = 0 WHERE module = 'user' AND delta = 'login' AND theme = 'restng_bootstrap'"

read -s -p "New admin password:" password
drush user-password admin --password="$password"
echo "password changed"
